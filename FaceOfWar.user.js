// ==UserScript==
// @name         Face of War
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       stretch
// @match        *://arms-of-war.com/game*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    addGlobalStyle('h5#chatHeader{display:none}#tasksContainer>h5{display:none;}#chatTabs>li{width:20%;text-align:center;}#castleToolTip{position:fixed;z-index:1000;width:20vw!important;left:40vw!important;max-width:none!important;min-width:0!important}#gameModifiers{margin-top:-10px;}nav{height:10vh!important;position:relative!important;}div#footer{display:none}body{padding:0!important;font-size:.83vw!important;}#bodyContainer{height:90vh;}#bodyContainer>.row:nth-child(1){height:50vh;}#bodyContainer>.row:nth-child(2){height:40vh;}#bodyContainer>.row:nth-child(3){display:none;}#chatContainer{height:49vh}#leaderboardContainer{height:38vh}#tasksContainer{height:49vh}#tasksContainer>br{display:none;}#storeContainer{height:38vh}#storeContainer>h5{display:none}#castleContainer{height:38vh}#castleContainer>div.mb-3{margin-bottom:0!important}#messageBox{margin-bottom:0!important;}#chatTabContent{height:calc(49vh - 83px)!important;margin-top:1px;}#chatTabContent>div{height:calc(49vh - 83px)!important;}#taskProgressBar{margin:0 auto}');
})();

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}